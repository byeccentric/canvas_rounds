"use strict"

class Round {
    constructor(x, y, r, color) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.color = color;
    }

    draw() {
        ctx.fillStyle = this.color;
        ctx.globalAlpha = 1;
        ctx.beginPath();
        ctx.arc((this.x+0.5)*width, (this.y+0.5)*width, this.r, 0, Math.PI*2, true);
        ctx.fill();
        return this;
    }
}

class Rect {
    constructor(x, y, width, height) {
        this.x = x; // координата х
        this.y = y; // координата у
        this.width = width; // ширина
        this.height = height; // высота
    }

    draw(options) {
        for(var key in options)
            ctx[key] = options[key];
        ctx.fillRect(this.x, this.y, this.width, this.height);
        return this;
    }
}

class Text {
	constructor(x, y, text) {
        this.x = x;
        this.y = y;
        this.text = text;
	}

	draw(options) {
        for(var key in options)
            ctx[key] = options[key];
        ctx.fillText(this.text, this.x, this.y);
        return this;
	}
}

class Line {
    constructor(x, y, xEnd, yEnd) {
        this.x = x;
        this.y = y;
        this.xEnd = xEnd;
        this.yEnd = yEnd;
    }

    draw(options) {
        ctx.beginPath();
        ctx.moveTo(this.x, this.y);
        ctx.lineTo(this.xEnd, this.yEnd);
        for(var key in options)
            ctx[key] = options[key];
        ctx.stroke();
        return this;
    }
}