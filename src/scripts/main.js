"use strict"

const colors  = ['#226bc4', '#ef463d', '#509efa', '#f39200', '#bd55bd', '#b61818', '#cccccc'];

var canvas = document.getElementById("container");
canvas.width = 1000;
canvas.height = 1200;
var ctx    = canvas.getContext('2d'),
    width  = 20,
    radius = (width - 4)/2,
    in_row = Math.floor(canvas.width/width),
    data   = [],
    rounds = [];
    /*desk   = new Rect(0,0,1000,600).draw({
        fillStyle: 'white',
        globalAlpha: 1
    }),*/

canvas.width = 1000;
canvas.height = 1200;

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

for(var i =0, rand=getRandomInt(100, 500); i<=rand; i++) {
    data.push({
        party_id: getRandomInt(0,7),
    });
}
data.sort((a,b) =>a.party_id - b.party_id);

for(var i = -1, item; item=data[++i];) {
    let color = colors[item.party_id];
    rounds.push(new Round(i%in_row, (i/in_row<<0)+2, radius, color).draw());
}
var text = new Text(500, 5, "Список депутатов по партиям").draw({textAlign: "center", textBaseline: "top", font: "22px bold", fillStyle: "black", strokeStyle: "black"});
var line = new Line(0, 35, 1000, 35).draw({strokeStyle: 'lightgrey', lineWidth: 1});
var line2 = new Line(0, 225, 1000, 225).draw({strokeStyle: 'lightgrey', lineWidth: 1});

rounds = [];
var allHeight = Math.ceil(data.length/50);
for(var i=-1, item; item=data[++i];) {
    let color = colors[item.party_id];
    rounds.push(new Round(i%in_row, (i/in_row<<0)+16, radius, color).draw());
}
text = new Text(500, 285, "Список депутатов по партиям").draw({textAlign: "center", textBaseline: "top", font: "22px bold", fillStyle: "black", strokeStyle: "black"});
line = new Line(0, 315, 1000, 315).draw({strokeStyle: 'lightgrey', lineWidth: 1});
line2 = new Line(0, (allHeight+16.3)*width, 1000, (allHeight+16.3)*width).draw({strokeStyle: 'lightgrey', lineWidth: 1});

rounds = [];
var fullSize = data.length/50;
var allHeight = Math.ceil(fullSize);
var leftInLast = Math.round((fullSize - allHeight)*-50);
var x = 0;
var y = 0;
for(var i=-1, item; item=data[++i];) {
    let color = colors[item.party_id];
    rounds.push(new Round(x, y+32, radius, color).draw());
    if((allHeight === y+1 && leftInLast > 0) || (allHeight === y+2 && leftInLast <= 0)) {
        y = -1;
        x++;
        leftInLast--;
    }
    y++;
}
text = new Text(500, 605, "Список депутатов по партиям").draw({textAlign: "center", textBaseline: "top", font: "22px bold Arial", fillStyle: "black", strokeStyle: "black"});
line = new Line(0, 635, 1000, 635).draw({strokeStyle: 'lightgrey', lineWidth: 1});
line2 = new Line(0, (allHeight+32.3)*width, 1000, (allHeight+32.3)*width).draw({strokeStyle: 'lightgrey', lineWidth: 1});


